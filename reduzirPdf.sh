#!/bin/bash

usage()
{
    cat << EOF
Uso:
$(basename $0) [-options] [estações]
    Compressor de pdf
    Ex:
    ./reduzirPdf.sh -c [1-5] origem.pdf destino.pdf
Opções:
    -h,        Mostra esse quadro de ajuda e finaliza.
    -c [1 a 5] Indica o nivel de compressão

EOF
    exit 0

}

while getopts "hc:" option; do
 case "${option}" in
          c) export levelnum=${OPTARG};;
          h) usage; exit;;
          *) usage; exit;;
 esac
done
shift $((OPTIND-1))

#echo $@
LEVEL=( screen ebook printer prepress default )

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/${LEVEL[$levelnum]} -dNOPAUSE -dQUIET -dBATCH -sOutputFile=$2 $1
