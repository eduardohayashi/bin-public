#!/bin/bash -   
#title          :countfiles.sh
#description    :Retorna a qtd de arquivos dentro de cada diretorio.  Não-recursivo.
#author         :Eduardo Hayashi
#date           :20160114
#version        :1.0
#usage          :./countfiles.sh
#notes          :       
#bash_version   :4.3.11(1)-release
#============================================================================

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

menu()
{
	echo "Usage: countfiles [DIR] [--light]"
	exit 0
}

countfiles()
{
find $1 -type d | while read dir; do
		echo -e ${RED}"$dir:"${NC} $( ls "$dir" | wc -l)
done
}

if [[ -z $@ ]] || [[ ! -d $1 ]]; then 
	menu
fi

countfiles $1