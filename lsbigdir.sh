#! /usr/bin/perl
#
# Author: Eduardo Hayashi
# Version: 0.1

use Cwd qw();
$dir = Cwd::cwd()."/".$ARGV[0];

chdir $dir or die; 

my @sizes=qw( B K MB GB TB PB);
sub nice_size
{
  my $size = shift;
  my $i = 0;

  while ($size > 1024)
  {
    $size = $size / 1024;
    $i++;
  }
  return sprintf("%.1f$sizes[$i]", $size);
}

use POSIX qw( strftime );
opendir D, $dir;
#dir; 

#print D;exit;

$c=1; 
while ($n = readdir D) {
	$mode=(stat $n)[2];
	$size=(stat $n)[7];
	$date=strftime("%Y-%m-%d %H:%M:%S",localtime((stat $n)[9]));
	printf "%o",$mode & 0777;
	print " ".nice_size($size)." ".$date." ".$n."\n"; 
	$c++
}


